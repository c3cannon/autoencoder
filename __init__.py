import numpy as np
from PIL import Image
#from . import models
from models import autoencoder
from torchvision.transforms import ToTensor
from torchvision.utils import save_image
from torch.autograd import Variable
from torch import nn
import torch

def to_img(x):
    x = 0.5 * (x + 1)
    x = x.clamp(0, 1)
    x = x.view(x.size(0), 3, 256, 256)
    return x

num_epochs = 10000
batch_size = 256
learning_rate = 1e-3

models = autoencoder()
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(models.parameters(), lr=learning_rate, weight_decay=1e-5)

for epoch in range(num_epochs):
    img = Image.open('/Users/claracannon/code/CS395T/proj1/data/01.jpg')
    img = img.convert(mode='RGB')
    img = ToTensor()(img).unsqueeze(0)
    img = Variable(img)
    # ===================forward=====================
    output = models(img)
    loss = criterion(output, img)
    # ===================backward====================
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()


pic = to_img(output.cpu().data)
save_image(pic, 'out.jpg')

#img = ToPILImage()(dec_img)
#Image.save(img, "out.jpg")
