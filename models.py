import torch
from torch import nn
"""
Optional: Your code here
"""
class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            torch.nn.Conv2d(3, 16, 3, stride=3, padding=1),  # b, 16, 10, 10
            torch.nn.ReLU(True),
            torch.nn.MaxPool2d(2, stride=2),  # b, 16, 5, 5
            torch.nn.Conv2d(16, 8, 3, stride=2, padding=1),  # b, 8, 3, 3
            torch.nn.ReLU(True),
            torch.nn.MaxPool2d(2, stride=1)  # b, 8, 2, 2
        )
        self.decoder = nn.Sequential(
            torch.nn.ConvTranspose2d(8, 16, 3, stride=2),  # b, 16, 5, 5
            torch.nn.ReLU(True),
            torch.nn.ConvTranspose2d(16, 8, 5, stride=3, padding=1),  # b, 8, 15, 15
            torch.nn.ReLU(True),
            #8, 1, 2
            torch.nn.ConvTranspose2d(8, 3, 2, stride=2, padding=1),  # b, 1, 28, 28
            torch.nn.Tanh()
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
